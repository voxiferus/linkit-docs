First Steps
===========

Getting started
---------------

1. By default the module is in AP mode.

2. Connect to the Linkit_Smart_xxx network and go to mylinkit.local URL

3. Set password and login to the web configurator

4. Select "Network" tab, switch to "Station mode" and connect the device to the local Wi-Fi network

Factory reset
-------------

1. Boot up the board and wait for the WiFi LED to go off.

2. Press and hold the WiFi button for at least 20 seconds then release.

3. The WiFi LED will blink fast for 1 second, and then reboot to perform a factory reset.


Update the firmware with a USB drive
------------------------------------

1. Download the latest .. _LinkIt Smart 7688 Firmware: https://docs.labs.mediatek.com/resource/linkit-smart-7688/en/downloads

2. Unzip and copy the image lks7688.img file to the root directory of a FAT32 USB drive.

3. Attach the USB drive to the HOST port of the board with an OTG cable.

4. Press and hold the on-board WiFi button.

5. Press the MPU reset button once while holding the WiFi button.

6. Keep holding the WiFi button for about 5 seconds. Do not release until the orange LED for WiFi is off. DO NOT press the WiFi button longer than 20s or it will upgrade the bootloader.

7. The WiFi LED blinks fast for about 3 minutes.

8. The device will automatically reboot after firmware update is complete.

Enabling SFTP
-------------

::

   # opkg update
   # opkg install vsftpd openssh-sftp-server
   # /etc/init.d/vsftpd enable
   # /etc/init.d/vsftpd start

Mounting the root FS on an SD card
----------------------------------

1. Enable ext4 support::

    # opkg update
    # opkg install block-mount kmod-fs-ext4 kmod-usb-storage-extras e2fsprogs fdisk

2. Format SD card::

    # mkfs.ext4 /dev/mmcblk0p1

3. Copy root fs to SD::

    # mount /dev/mmcblk0p1 /mnt
    # tar -C /overlay -cvf - . | tar -C /mnt -xf -
    # umount /mnt

4. Generate *fstab* (file system partitions table) from the current fs configuration::

    # block detect > /etc/config/fstab

5. Edit *fstab*::

    config 'mount'
      option  target  '/overlay'
      ...
      option  enabled '1'

6. Reboot