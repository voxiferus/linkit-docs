Peripheral
==========

Pinmux
------

1. List pin groups::

    mt7688_pinmux get

2. Reassign pin group::

    mt7688_pinmux set <group> <function>


SPI over GPIO
-------------

1. Install SPI-GPIO drivers::

    # opkg update
    # opkg install kmod-spi-gpio
    # opkg install kmod-spi-dev
    # opkg install kmod-spi-gpio-custom

2. Use mt7688_pinmux to make sure devoted pins are in GPIO state. Probably, reboot needed ???

2. Insert the module::

    insmod spi-gpio-custom bus<x=0...3>=<id>,<sck>,<mosi>,<miso>,<mode1>,<maxfreq1>,<cs1>

E.g.::

    insmod spi-gpio-custom bus0=1,14,15,16,0,10000,17

Modes:

+------------+------------+-----------+
| Mode#      | CPOL       | CPHA      |
+============+============+===========+
| 0          | 0          | 0         |
+------------+------------+-----------+
| 1          | 0          | 1         |
+------------+------------+-----------+
| 2          | 1          | 0         |
+------------+------------+-----------+
| 3          | 1          | 1         |
+------------+------------+-----------+