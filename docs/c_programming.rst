C/C++ Programming
=================

Cross compilation
-----------------

1. Download SKD from .. _Mediatek downloads page: https://docs.labs.mediatek.com/resource/linkit-smart-7688/en/downloads e.g.::

    wget https://labs.mediatek.com/en/download/AGSSkG38

2. Unpack::

    sudo tar -xvjf SDK.tar.bz2



Node.js native addons cross-compilation
---------------------------------------

1. Fork the module repo

2. Clone and edit unnecessary dependencies like nan

3. Deploy SKD if needed. Set $STAGING_DIR environment variable, e.g.::

     export STAGING_DIR=/sdk/OpenWrt-SDK-ramips-mt7688_gcc-4.8-linaro_uClibc-0.9.33.2.Linux-x86_64/staging_dir

4. Run the following bash script in the module folder::

    set -e

    if [ ! -d "$STAGING_DIR" ]; then
        echo "STAGING_DIR needs to be set to your cross toolchain path";
        exit 1
    fi

    ARCH=${ARCH:-mipsel}
    NODE=${NODE:-0.12.7}

    TOOLCHAIN_DIR=$(ls -d "$STAGING_DIR/toolchain-"*)
    echo $TOOLCHAIN_DIR

    export SYSROOT=$(ls -d "$STAGING_DIR/target-"*)

    source $TOOLCHAIN_DIR/info.mk # almost a bash script

    echo "Cross-compiling for" $TARGET_CROSS

    export PATH=$TOOLCHAIN_DIR/bin:$PATH
    export CPPPATH=$TARGET_DIR/usr/include
    export LIBPATH=$TARGET_DIR/usr/lib

    #TODO: anything better than this hack?
    OPTS="-I $SYSROOT/usr/include -L $TOOLCHAIN_DIR/lib -L $SYSROOT/usr/lib"

    export CC="${TARGET_CROSS}gcc $OPTS"
    export CXX="${TARGET_CROSS}g++ $OPTS"
    export AR=${TARGET_CROSS}ar
    export RANLIB=${TARGET_CROSS}ranlib
    export LINK="${TARGET_CROSS}g++ $OPTS"
    export CPP="${TARGET_CROSS}gcc $OPTS -E"
    export STRIP=${TARGET_CROSS}strip
    export OBJCOPY=${TARGET_CROSS}objcopy
    export LD="${TARGET_CROSS}g++ $OPTS"
    export OBJDUMP=${TARGET_CROSS}objdump
    export NM=${TARGET_CROSS}nm
    export AS=${TARGET_CROSS}as

    export npm_config_arch=$ARCH
    node-gyp rebuild --target=$NODE -v

5. Remove /build from .gitignore, commit and push.

6. Install directly from GitHub, e.g.::

    npm install https://github.com/Voxiferus/node-spi.git --global

